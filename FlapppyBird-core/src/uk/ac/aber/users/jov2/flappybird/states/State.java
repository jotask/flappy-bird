package uk.ac.aber.users.jov2.flappybird.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract interface State {
	
	public abstract void init();
	public abstract void update(float delta);
	public abstract void render(SpriteBatch sb);
	public abstract void dispose();

}

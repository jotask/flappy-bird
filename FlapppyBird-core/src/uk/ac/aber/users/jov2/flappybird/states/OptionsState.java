package uk.ac.aber.users.jov2.flappybird.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import uk.ac.aber.users.jov2.flappybird.FlappyBird;
import uk.ac.aber.users.jov2.flappybird.util.GameStateManager.StateHolder;

public class OptionsState implements State {
	
	private Texture bg;
	
	private OrthographicCamera cam;
	
	private Stage stage;
	private Table table;

	@Override
	public void init() {
		cam = new OrthographicCamera(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		bg = new Texture(Gdx.files.internal("bg.png"));
		
		stage = new Stage();
		table = new Table();
		table.setFillParent(true);
		table.setDebug(true);
		stage.addActor(table);
		
		final TextButton exit = new TextButton("Exit", FlappyBird.skin);
		exit.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				FlappyBird.gsm.changeState(StateHolder.MENU);
			}
		});
		table.add(exit).fill();
		
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void update(float delta) {
		table.setDebug(FlappyBird.debug);
	}

	@Override
	public void render(SpriteBatch sb) {
		sb.setProjectionMatrix(cam.combined);
		sb.begin();
		sb.draw(bg, -cam.viewportWidth / 2, -cam.viewportHeight / 2, cam.viewportWidth, cam.viewportHeight);
		sb.end();
		stage.draw();
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}

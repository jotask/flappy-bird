package uk.ac.aber.users.jov2.flappybird.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Stage;

import uk.ac.aber.users.jov2.flappybird.game.Game;
import uk.ac.aber.users.jov2.flappybird.game.GameOver;
import uk.ac.aber.users.jov2.flappybird.game.Idle;

public class GameState implements State {
	
	public enum StateGame {IDLE, RUN, GAMEOVER};
	public StateGame currentState;
	private ShapeRenderer sr;
	private boolean debug = false;
	private Stage stage;
	private OrthographicCamera cam;
	private Game game;
	private Idle idle;
	private GameOver gameOver;

	@Override
	public void init() {
		sr =  new ShapeRenderer();
		cam = new OrthographicCamera();
		cam.setToOrtho(false, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		
	
		stage = new Stage();
		
		idle = new Idle(this, stage);
		game = new Game(this, cam);
		gameOver = new GameOver(this, stage);
		
		currentState = StateGame.IDLE;
		enter();
	}

	@Override
	public void update(float delta) {
		if(Gdx.input.isKeyJustPressed(Keys.Q))debug = !debug;
		switch(currentState){
		case IDLE:
			idleUpdate(delta);
			break;
		case RUN:
			runUpdate(delta);
			break;
		case GAMEOVER:
			gameOverUpdate(delta);
			break;
		}
	}

	private void gameOverUpdate(float delta) {
		gameOver.update(delta);
	}

	private void runUpdate(float delta) {
		game.update(delta);
	}

	private void idleUpdate(float delta) {
		idle.update(delta);
	}

	@Override
	public void render(SpriteBatch sb) {
		switch(currentState){
		case IDLE:
			idleRender(sb);
			break;
		case RUN:
			runRender(sb);
			break;
		case GAMEOVER:
			gameOverRender(sb);
			break;
		}
		if(debug)
			debug();
	}

	private void gameOverRender(SpriteBatch sb) {
		game.render(sb);
		gameOver.render(sb);
	}

	private void runRender(SpriteBatch sb) {
		game.render(sb);
	}

	private void idleRender(SpriteBatch sb) {
		game.render(sb);
		idle.render(sb);
	}
	
	private void debug(){
		sr.setProjectionMatrix(cam.combined);
		sr.begin(ShapeType.Line);
		sr.setColor(Color.RED);
				game.debugRender(sr);
		sr.end();
	}

	@Override
	public void dispose() {
		game.dispose();
	}
	
	private void enter(){
		switch(currentState){
		case IDLE:
			idle.enter();
			break;
		case RUN:
			game.enter();
			break;
		case GAMEOVER:
			gameOver.enter();
			break;
		}
	}
	
	private void exit(){
		switch(currentState){
		case IDLE:
			idle.exit();
			break;
		case RUN:
			game.exit();
			break;
		case GAMEOVER:
			gameOver.exit();
			break;
		}
	}
	
	public void setState(StateGame sg){
		exit();
		this.currentState = sg;
		enter();
	}
	
	public void resetGame(){
		cam = new OrthographicCamera();
		cam.setToOrtho(false, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		game = new Game(this, cam);
		setState(StateGame.IDLE);
	}
	
}

package uk.ac.aber.users.jov2.flappybird.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Tube {
	
	public static float WIDTH;
	public static final float MARGIN = 50f;
	
	private static Texture top, bottom;
	private Rectangle recTop, recBottom;
	
	private Vector2 position;
	private float space;
	
	public Tube(Vector2 p) {
		top = new Texture(Gdx.files.internal("toptube.png"));
		bottom = new Texture(Gdx.files.internal("bottomtube.png"));
		
		WIDTH = top.getWidth();
		
		moveTobe(p);
	}
	
	public void render(SpriteBatch sb){
		sb.draw(top, recTop.x, recTop.y);
		sb.draw(bottom, recBottom.x, recBottom.y);
	}
	
	public void debugRender(ShapeRenderer sr){
		sr.box(recTop.x, recTop.y, 0, recTop.width, recTop.height, 0);
		sr.box(recBottom.x, recBottom.y, 0, recBottom.width, recBottom.height, 0);
	}
	
	public boolean collide(Rectangle bird){
		
		if(recTop.overlaps(bird) || recBottom.overlaps(bird))return true;
		
		return false;
	}
	
	public void moveTobe(Vector2 pos){
		
		this.position = pos;
		
		float bird = Bird.bird.getWidth();
		space = MathUtils.random(bird, bird * 3);
		
		recTop = new Rectangle();
		recTop.setPosition(position.x, position.y + space);
		recTop.setSize(bottom.getWidth(), bottom.getHeight());
		
		recBottom =  new Rectangle();
		recBottom.setPosition(position.x, (position.y - bottom.getHeight()) - space);
		recBottom.setSize(bottom.getWidth(), bottom.getHeight());
	}
	
	public static void dispose(){
		top.dispose();
		bottom.dispose();
	}
	
	public Vector2 getPosition(){
		return this.position;
	}

}

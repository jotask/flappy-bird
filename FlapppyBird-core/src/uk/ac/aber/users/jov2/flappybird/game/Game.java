package uk.ac.aber.users.jov2.flappybird.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import uk.ac.aber.users.jov2.flappybird.states.GameState;
import uk.ac.aber.users.jov2.flappybird.states.StateGame;

public class Game implements StateGame{
	
	private GameState gs;
	private static final int MANY_TUBES = 3;
	private static final float TUBE_SPACE = 100;
	
	private final InputProcessor processor;
	private OrthographicCamera cam;
	private Bird bird;
	private Background back;
	private Tube[] tube;
	
	public Game(GameState gs, OrthographicCamera cam) {
		this.gs = gs;
		this.cam = cam;
		back = new Background();
		bird = new Bird(0 + Bird.OFFSET, cam.position.y);
		
		processor = Gdx.input.getInputProcessor();
		
		tube = new Tube[MANY_TUBES];
		
		float x = cam.position.x;
		float y = cam.position.y;
		for(int i = 0; i < tube.length; i++){
			x += TUBE_SPACE + Tube.WIDTH;
			tube[i] = new Tube(new Vector2(x, y));
		}
		System.out.println(cam.position.x + " - " + cam.position.y);
	}

	@Override
	public void update(float delta) {
		
		back.update(cam, delta);
		
		// Logic for the bird and update the camera
		if(Gdx.input.justTouched())bird.jump();
		bird.update(delta);
		cam.position.set(bird.getPosition().x + Bird.OFFSET, cam.position.y, 0);
		cam.update();
		
		// Check collisions and the tube position. If the tube is out off screen in the left size, just repositionate the tube
		float outScreen = (cam.position.x - (cam.viewportWidth / 2) - Tube.WIDTH);
		for(int i = 0; i < tube.length; i++){
			Tube t = tube[i];
			if(t.getPosition().x < outScreen){
				Vector2 p = new Vector2(t.getPosition().x + (Tube.WIDTH + TUBE_SPACE) * MANY_TUBES, cam.position.y);
				t.moveTobe(p);
			}
			if(t.collide(bird.getBounds())){
				gs.setState(GameState.StateGame.GAMEOVER);
			}
		}
	}

	@Override
	public void render(SpriteBatch sb) {
		sb.setProjectionMatrix(cam.combined);
		sb.begin();
		back.render(sb);
		for(int i = 0; i < tube.length; i++) tube[i].render(sb);
		bird.render(sb);
		sb.end();
	}
	
	@Override
	public void debugRender(ShapeRenderer sr){
		back.debugRender(sr);
		for(int i = 0; i < tube.length; i++) tube[i].debugRender(sr);
		bird.renderDebug(sr);
	}

	public void dispose() {
		bird.dispose();
		back.dispose();
		Tube.dispose();
	}

	@Override
	public void enter() {
		Gdx.input.setInputProcessor(processor);
	}

	@Override
	public void exit() { }

}

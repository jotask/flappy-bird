package uk.ac.aber.users.jov2.flappybird.util;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import uk.ac.aber.users.jov2.flappybird.states.EmptyState;
import uk.ac.aber.users.jov2.flappybird.states.GameState;
import uk.ac.aber.users.jov2.flappybird.states.MenuState;
import uk.ac.aber.users.jov2.flappybird.states.OptionsState;
import uk.ac.aber.users.jov2.flappybird.states.SplashState;
import uk.ac.aber.users.jov2.flappybird.states.State;

public class GameStateManager {
	
	public enum StateHolder { EMPTY, SPLASH, MENU, OPTIONS, GAME };
	
	private HashMap<StateHolder, State> states;
	
	private StateHolder currentState;
	
	public GameStateManager() {
		states = new HashMap<StateHolder, State>();
		states.put(StateHolder.EMPTY, new EmptyState());
		states.put(StateHolder.SPLASH, new SplashState());
		states.put(StateHolder.MENU, new MenuState());
		states.put(StateHolder.OPTIONS, new OptionsState());
		states.put(StateHolder.GAME, new GameState());
		
		// TODO change to an Empty State for default
		currentState = StateHolder.GAME;
		states.get(currentState).init();
	}
	
	public void update(float delta){
		states.get(currentState).update( delta);
	}
	
	public void render(SpriteBatch sb){
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
		states.get(currentState).render(sb);
	}
	
	public void changeState(StateHolder st){
		this.changeState(st, false);
	}
	
	public void changeState(StateHolder st, boolean remove){
		states.get(currentState).dispose();
		if(remove)states.remove(currentState);
		currentState = st;
		states.get(currentState).init();
	}
	
	public void dispose(){
		states.clear();
	}

}

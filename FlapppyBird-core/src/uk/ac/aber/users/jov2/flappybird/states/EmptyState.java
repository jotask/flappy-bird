package uk.ac.aber.users.jov2.flappybird.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import uk.ac.aber.users.jov2.flappybird.FlappyBird;
import uk.ac.aber.users.jov2.flappybird.util.GameStateManager.StateHolder;

public class EmptyState implements State {
	
	@Override
	public void init() { }

	@Override
	public void update(float delta) {
		FlappyBird.gsm.changeState(StateHolder.SPLASH, true);
	}

	@Override
	public void render(SpriteBatch sb) { }

	@Override
	public void dispose() { }

}

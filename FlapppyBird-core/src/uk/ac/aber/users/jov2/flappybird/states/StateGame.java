package uk.ac.aber.users.jov2.flappybird.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public abstract interface StateGame {
	
	public abstract void update(float delta);
	
	public abstract void render(SpriteBatch sb);
	
	public abstract void debugRender(ShapeRenderer sr);
	
	public abstract void enter();
	public abstract void exit();

}

package uk.ac.aber.users.jov2.flappybird.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Bird {
	
	private static final float GRAVITY = -15;
	private static final float MOVEMENT = 50; // 20
	private static final float JUMP = 250;
	public static final float OFFSET = 60;
	public static Texture bird;
	private Rectangle bound;
	private Vector2 position;
	private Vector2 velocity;
	
	public Bird(float x, float y) {
		bird = new Texture(Gdx.files.internal("bird.png"));
		
		position = new Vector2(x, y - (bird.getHeight() / 2));
		bound = new Rectangle();
		bound.setPosition(position.x, position.y);
		bound.setSize(bird.getWidth(), bird.getHeight());
		velocity = new Vector2();
	}
	
	public void jump(){
		velocity.y = JUMP;
	}
	
	public void update(float delta){
		velocity.add(0, GRAVITY);
		velocity.scl(delta);
		position.add(velocity.x, velocity.y);
		velocity.scl(1/delta);
		position.add(MOVEMENT * delta, 0);

		if(position.y < 0)position.y = 0;
		
		bound.setPosition(position);
	}
	
	public void render(SpriteBatch sb){
		sb.draw(bird, bound.x, bound.y);
	}
	
	public void renderDebug(ShapeRenderer sr){
		sr.box(bound.x, bound.y, 0, bound.width, bound.height, 0);
	}
	
	public void dispose(){
		bird.dispose();
	}
	
	public Vector2 getPosition(){ return position; }
	public Rectangle getBounds(){ return bound; }
}

package uk.ac.aber.users.jov2.flappybird.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import uk.ac.aber.users.jov2.flappybird.FlappyBird;
import uk.ac.aber.users.jov2.flappybird.states.GameState;
import uk.ac.aber.users.jov2.flappybird.states.StateGame;

public class Idle implements StateGame{
	
	private Stage stage;
	private Skin skin;
	private Table table;
	private GameState gs;

	public Idle(GameState gs, Stage stage) {
		this.gs = gs;
		this.stage = stage;
		this.skin = FlappyBird.skin;
		
		table = new Table(skin);
		table.setFillParent(true);
		table.setDebug(true);
		
		Label label = new Label("Play", this.skin);
		table.add(label);
		
	}

	@Override
	public void update(float delta) {
		table.setDebug(FlappyBird.debug);
		if(Gdx.input.justTouched())
			gs.setState(GameState.StateGame.RUN);
	}

	@Override
	public void render(SpriteBatch sb) {
		stage.draw();
	}

	@Override
	public void debugRender(ShapeRenderer sr) { }

	@Override
	public void enter() {
		stage.addActor(table);
	}

	@Override
	public void exit() {
		table.remove();
	}
	
}

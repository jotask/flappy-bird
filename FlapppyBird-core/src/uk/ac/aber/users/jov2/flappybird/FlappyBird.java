package uk.ac.aber.users.jov2.flappybird;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import uk.ac.aber.users.jov2.flappybird.util.GameStateManager;

public class FlappyBird extends ApplicationAdapter {
	
	private SpriteBatch sb;
	public static  GameStateManager gsm;
	public static Skin skin;
	
	public static boolean debug;
	
	@Override
	public void create () {
		sb = new SpriteBatch();
		skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
		gsm = new GameStateManager();
	}

	@Override
	public void render () {
		if(Gdx.input.isKeyJustPressed(Keys.TAB))
			debug = !debug;
		gsm.update(Gdx.graphics.getDeltaTime());
		gsm.render(sb);
	}
	 @Override
	public void dispose() {
		gsm.dispose();
		sb.dispose();
	}
}

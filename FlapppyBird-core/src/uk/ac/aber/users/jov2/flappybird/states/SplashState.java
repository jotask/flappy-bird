package uk.ac.aber.users.jov2.flappybird.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import uk.ac.aber.users.jov2.flappybird.FlappyBird;
import uk.ac.aber.users.jov2.flappybird.util.GameStateManager.StateHolder;

public class SplashState implements State {

	private OrthographicCamera cam;
	
	private Texture aiko;
	private static final float TIME = 3f;
	private float passedTime;
	private Rectangle logo;
	
	@Override
	public void init() {
		cam = new OrthographicCamera();
		cam.setToOrtho(false, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		aiko = new Texture(Gdx.files.internal("aiko.png"));
		logo = new Rectangle();
		logo.setPosition(cam.viewportWidth / 2 - (aiko.getWidth() / 2), cam.viewportHeight / 2  - (aiko.getHeight() / 2));
		logo.setSize(aiko.getWidth(), aiko.getHeight());
	}

	@Override
	public void update(float delta) {
		if(passedTime > TIME){
			FlappyBird.gsm.changeState(StateHolder.MENU, true);
			return;
		}else{
			passedTime += delta;
		}
	}

	@Override
	public void render(SpriteBatch sb) {
		sb.setProjectionMatrix(cam.combined);
		sb.begin();
		sb.draw(aiko, logo.x, logo.y, logo.width, logo.height);
		sb.end();
	}

	@Override
	public void dispose() {
		aiko.dispose();
	}
	
}
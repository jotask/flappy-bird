package uk.ac.aber.users.jov2.flappybird.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import uk.ac.aber.users.jov2.flappybird.FlappyBird;
import uk.ac.aber.users.jov2.flappybird.states.GameState;
import uk.ac.aber.users.jov2.flappybird.states.StateGame;

public class GameOver implements StateGame{
	
	private Stage stage;
	private Skin skin;
	private Table table;
	@SuppressWarnings("unused")
	private GameState gs;
	
	private InputProcessor processor;

	public GameOver(final GameState gs, Stage stage) {
		this.gs = gs;
		this.stage = stage;
		this.skin = FlappyBird.skin;
		
		table = new Table(skin);
		table.setFillParent(true);
		
		final TextButton replay = new TextButton("replay", skin);
		replay.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				gs.resetGame();
			}
		});
		table.add(replay).row();;
		
		Label label = new Label("GameOver", this.skin);
		table.add(label).row();
		
		final TextButton menu = new TextButton("menu", skin);
		menu.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				
			}
		});
		table.add(menu);
		
	}

	@Override
	public void update(float delta) {
		table.setDebug(FlappyBird.debug);
		stage.act(delta);
	}

	@Override
	public void render(SpriteBatch sb) {
		stage.draw();
	}

	@Override
	public void debugRender(ShapeRenderer sr) { }

	@Override
	public void enter() {
		processor = Gdx.input.getInputProcessor();
		Gdx.input.setInputProcessor(stage);
		stage.addActor(table);
	}

	@Override
	public void exit() {
		table.remove();
		Gdx.input.setInputProcessor(processor);
	}

}

package uk.ac.aber.users.jov2.flappybird.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

public class Background {
	
	private static Texture bg;
	private Vector2 p1, p2;
	
	public Background() {
		bg = new Texture(Gdx.files.internal("bg.png"));
		p1 = new Vector2(0, 0);
		p2 = new Vector2(bg.getWidth(), 0);
	}
	
	public void update(OrthographicCamera  cam, float delta){
		// Check if the background image is out of screen and change his position
		if(cam.position.x - (cam.viewportWidth / 2) > p1.x + bg.getWidth()){
			p1.x = p2.x + bg.getWidth();
		}else if(cam.position.x - (cam.viewportWidth / 2) > p2.x + bg.getWidth()){
			p2.x = p1.x + bg.getWidth();
		}
		
	}
	
	public void render(SpriteBatch sb){
		sb.draw(bg, p1.x, p2.y);
		sb.draw(bg, p2.x, p2.y);
	}
	
	public void debugRender(ShapeRenderer sr){
		sr.box(p1.x, p1.y, 0, bg.getWidth(), bg.getHeight(), 0);
		sr.box(p2.x, p2.y, 0, bg.getWidth(), bg.getHeight(), 0);
	}
	
	public void dispose(){
		bg.dispose();
	}

}

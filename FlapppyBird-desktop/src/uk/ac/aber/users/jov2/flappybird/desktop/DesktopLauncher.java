package uk.ac.aber.users.jov2.flappybird.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import uk.ac.aber.users.jov2.flappybird.FlappyBird;

public class DesktopLauncher {
	
	public static void main (String[] arg) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		// TODO change to real size, now is reduced for be visible on streamings
		// Plus 100
		cfg.width = 380;
		cfg.height = 700;
		cfg.fullscreen = false;
		cfg.title = "Flappy Bird Clone";
		cfg.resizable = false;
		
		new LwjglApplication(new FlappyBird(), cfg);
	}
}

package uk.ac.aber.users.jov2.flappybird.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import uk.ac.aber.users.jov2.flappybird.FlappyBird;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
		
		cfg.useAccelerometer = false;
		cfg.useCompass = false;
		
		initialize(new FlappyBird(), cfg);
	}
}
